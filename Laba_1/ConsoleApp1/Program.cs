﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteBook
{
    public class Program
    {
        public static void Main()
        {
            Console.WriteLine("Я записная книжка. Что ты хочешь сделать?\n");
            Functions func = new Functions();
            func.Switch();
        }
    }

    public class Functions
    {
        public List<string> lastName = new List<string>();
        public List<string> name = new List<string>();
        public List<string> country = new List<string>();
        public List<string> number = new List<string>();

        public List<string> prename = new List<string>();
        public List<string> date = new List<string>();
        public List<string> org = new List<string>();
        public List<string> dol = new List<string>();
        public List<string> other = new List<string>();

        public void Switch()
        {
            Console.WriteLine(" <1> - Добавить новую запись\n <2> - Редактировать записи\n <3> - Удалить записи\n <4> - Просмотреть созданные записи\n <5> - Просмотр краткой информации о записях\n <0> - Выйти из книжки\n");
            string a = Console.ReadLine();

            switch (a)
            {
                case "1":
                    Add();
                    Switch();
                    break;

                case "2":
                    Edit();
                    if (lastName.Count > 0) Switch();
                    break;

                case "3":
                    Remote();
                    if (lastName.Count > 0) Switch();
                    break;

                case "4":
                    SeeAll();
                    if (lastName.Count > 0) Switch();
                    break;

                case "5":
                    See();
                    if (lastName.Count > 0) Switch();
                    break;

                case "0":
                    Console.WriteLine("Выход");
                    break;

                default:
                    Console.WriteLine("\nНе правильная команда, попробуйте снова!!!");
                    Switch();
                    break;
            }
        }

        public void Add()
        {
            string a;

            Console.WriteLine("\nФамилия:");
        LastName:
            a = Console.ReadLine();
            if (a.ToCharArray().All(char.IsLetter))
            {
                lastName.Add(a);
                while (a == "" || a == null)
                {
                    Console.WriteLine("Это обязательное поле введите его");
                    lastName.Remove(a);
                    a = Console.ReadLine();
                    lastName.Add(a);
                }
            }
            else
            {
                Console.WriteLine("Не верный ввод Фамилии. Только буквы.");
                goto LastName;
            }

            Console.WriteLine("\nИмя:");
        Name:
            a = Console.ReadLine();
            if (a.ToCharArray().All(char.IsLetter))
            {
                name.Add(a);
                while (a == "" || a == null)
                {
                    Console.WriteLine("Это обязательное поле введите его");
                    name.Remove(a);
                    a = Console.ReadLine();
                    name.Add(a);
                }
            }
            else
            {
                Console.WriteLine("Не верный ввод Имени. Только буквы.");
                goto Name;
            }

            Console.WriteLine("\nОтчество (при наличии):");
        PreName:
            if (a.ToCharArray().All(char.IsLetter))
            {
                prename.Add(Console.ReadLine());
            }
            else
            {
                Console.WriteLine("Не верный ввод Отчества. Только буквы.");
                goto PreName;
            }

            Console.WriteLine("\nНомер телефона:");
        Number:
            a = Console.ReadLine();
            if (a.ToCharArray().All(char.IsDigit))
            {
                number.Add(a);
                while (a == "" || a == null)
                {
                    Console.WriteLine("Это обязательное поле введите его");
                    number.Remove(a);
                    a = Console.ReadLine();
                    number.Add(a);
                }
            }
            else
            {
                Console.WriteLine("Не верный ввод Номера. Только цифры.");
                goto Number;
            }

            Console.WriteLine("\nСтрана:");
        State:
            a = Console.ReadLine();
            if (a.ToCharArray().All(char.IsLetter))
            {
                country.Add(a);
                while (a == "" || a == null)
                {
                    Console.WriteLine("Это обязательное поле введите его");
                    country.Remove(a);
                    a = Console.ReadLine();
                    country.Add(a);
                }
            }
            else
            {
                Console.WriteLine("Не верный ввод Страны. Только буквы.");
                goto State;
            }
            Console.WriteLine("\nДата рождения(ДДММГГГГ) (не обязательно):");
        Date:
            a = Console.ReadLine();
            if (a.ToCharArray().All(char.IsDigit))
            {
                date.Add(a);
            }
            else
            {
                Console.WriteLine("Не верный ввод даты. Только цифры.");
                goto Date;
            }

            Console.WriteLine("\nОрганизация (не обязательно):");
            org.Add(Console.ReadLine());

            Console.WriteLine("\nДолжность (не обязательно):");
            dol.Add(Console.ReadLine());

            Console.WriteLine("\nПрочие заметки (не обязательно):");
            other.Add(Console.ReadLine());
        }

        public void Edit()
        {
            if (lastName.Count > 0)
            {
            Edit:
                Console.WriteLine("\nДоступные записи: ");
                for (int i =

                0; i < lastName.Count; i++)
                {
                    Console.Write("№ " + i + " \n");
                }
                Console.WriteLine("Какой номер записи вы хотите изменить? (Введите номер)\n");
                try
                {
                    int c = int.Parse(Console.ReadLine());

                    if (c <= lastName.Count)
                    {
                        if ((prename[c] == "") || (prename[c] == null))
                        {
                            prename[c] = "Не имеется";
                        }

                        if ((org[c] == "") || (org[c] == null))
                        {
                            org[c] = "Не имеется";
                        }

                        if ((dol[c] == "") || (dol[c] == null))
                        {
                            dol[c] = "Не имеется";
                        }

                        if ((other[c] == "") || (other[c] == null))
                        {
                            other[c] = "Не имеется";
                        }

                        if ((date[c] == "") || (date[c] == null))
                        {
                            date[c] = "Не имеется";
                        }

                        Console.WriteLine($"Данные по записе номер {c}:\n");
                        Console.WriteLine($"1. Фамилия: {lastName[c]}");
                        Console.WriteLine($"2. Имя: {name[c]}");
                        Console.WriteLine($"3. Отчество: {prename[c]}");
                        Console.WriteLine($"4. Страна: {country[c]}");
                        Console.WriteLine($"5. Номер телефона: {number[c]}");
                        Console.WriteLine($"6. Дата рождения: {date[c]}");
                        Console.WriteLine($"7. Организация: {org[c]}");
                        Console.WriteLine($"8. Должность: {dol[c]}");
                        Console.WriteLine($"9. Прочее: {other[c]}");

                    Switch:
                        Console.WriteLine("\nЧто изменить?(Номер)");

                        string d = Console.ReadLine();

                        Console.WriteLine("Введите новые данные для изменения");
                        switch (d)
                        {
                            case "1":
                                lastName[c] = Console.ReadLine();
                                break;
                            case "2":
                                name[c] = Console.ReadLine();
                                break;
                            case "3":
                                prename[c] = Console.ReadLine();
                                break;
                            case "4":
                                country[c] = Console.ReadLine();
                                break;
                            case "5":
                                number[c] = Console.ReadLine();
                                break;
                            case "6":
                                date[c] = Console.ReadLine();
                                break;
                            case "7":
                                org[c] = Console.ReadLine();
                                break;
                            case "8":
                                dol[c] = Console.ReadLine();
                                break;
                            case "9":
                                other[c] = Console.ReadLine();
                                break;
                            default:
                                Console.WriteLine("Введен не правильный номер!!!");
                                goto Switch;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Ошибка выбора записи! Попробуйте снова\n");
                        goto Edit;
                    }
                }
                catch (System.FormatException)
                {
                    Console.WriteLine("Ошибка выбора записи! Попробуйте снова\n");
                    goto Edit;
                }
            }
            else
            {
                Console.WriteLine("Нет элементов для изменения\n");
                Switch();
            }
        }

        public void Remote()
        {
            if (lastName.Count > 0)
            {
            Remote:
                Console.WriteLine("\nДоступные записи: ");
                for (int i = 0; i < lastName.Count; i++)
                {
                    Console.Write("№ " + i + " \n");
                }
                Console.WriteLine("Какой номер записи вы хотите удалить? (Введите номер)\n");
                try
                {
                    int c = int.Parse(Console.ReadLine());

                    if (c <= lastName.Count)
                    {
                        for (int i = 0; i < lastName.Count; i++)
                        {
                            lastName.RemoveAt(c);
                            name.RemoveAt(c);
                            prename.RemoveAt(c);
                            country.RemoveAt(c);
                            number.RemoveAt(c);
                            date.RemoveAt(c);
                            org.RemoveAt(c);
                            dol.RemoveAt(c);
                            other.RemoveAt(c);
                            Console.WriteLine($"Созданная запись №{i} удалена\n");
                            Switch();
                        }
                    }
                    else
                    {
                        Console.WriteLine("Ошибка выбора записи! Попробуйте снова\n");
                        goto Remote;
                    }
                }
                catch (System.FormatException)
                {
                    Console.WriteLine("Ошибка выбора записи! Попробуйте снова\n");
                    goto Remote;
                }
            }
            else
            {
                Console.WriteLine("Нечего удалять\n");
                Switch();
            }
        }

        public void SeeAll()
        {
            if (lastName.Count > 0)
            {
                for (int i = 0; i < lastName.Count; i++)
                {
                    Console.WriteLine("\nЗапись№ " + i);
                    Console.Write("Фамилия: ");
                    if (lastName[i] == "" || lastName[i] == null)
                    {
                        Console.WriteLine("Ошибка!!!");
                    }
                    else
                    {
                        Console.WriteLine(lastName[i]);
                    }

                    Console.Write("Имя: ");
                    if (name[i] == "" || name[i] == null)
                    {
                        Console.WriteLine("Ошибка!!!");
                    }
                    else
                    {
                        Console.WriteLine(name[i]);
                    }

                    Console.Write("Отчество: ");
                    if ((prename[i] == "") || (prename[i] == null))
                    {
                        Console.WriteLine("Не имеется");
                    }
                    else
                    {
                        Console.WriteLine(prename[i]);
                    }

                    Console.Write("Страна: ");
                    if (country[i] == "" || country[i] == null)
                    {
                        Console.WriteLine("Ошибка!!!");
                    }
                    else
                    {
                        Console.WriteLine(country[i]);
                    }

                    Console.Write("Дата рождения: ");
                    if (date[i] == "" || date[i] == null)
                    {
                        Console.WriteLine("Не имеется");
                    }
                    else
                    {
                        Console.WriteLine(date[i]);
                    }

                    Console.Write("Номер телефона: ");
                    if (number[i] == "" || number[i] == null)
                    {

                        Console.WriteLine("Ошибка!!!");
                    }
                    else
                    {
                        Console.WriteLine(number[i]);
                    }

                    Console.Write("Организация: ");
                    if (org[i] == "" || org[i] == null)
                    {
                        Console.WriteLine("Не имеется");
                    }
                    else
                    {
                        Console.WriteLine(org[i]);
                    }

                    Console.Write("Должность: ");
                    if (dol[i] == "" || dol[i] == null)
                    {
                        Console.WriteLine("Не имеется");
                    }
                    else
                    {
                        Console.WriteLine(dol[i]);
                    }

                    Console.Write("Доп инфа: ");
                    if (other[i] == "" || other[i] == null)
                    {
                        Console.WriteLine("Не имеется");
                    }
                    else
                    {
                        Console.WriteLine(other[i]);
                    }
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("В данный момент записей не имеется\n");
                Switch();
            }
        }

        public void See()
        {
            if (lastName.Count > 0)
            {
                for (int i = 0; i < lastName.Count; i++)
                {
                    Console.WriteLine("\nЗапись №" + (i));
                    Console.WriteLine($"Фамилия: {lastName[i]}\nИмя: {name[i]}\nНомер телефона: {number[i]}");
                    if (i == lastName.Count - 1) Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("В данный момент записей не имеется\n");
                Switch();
            }
        }
    }
}